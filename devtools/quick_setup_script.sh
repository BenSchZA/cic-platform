#!/usr/bin/env bash
set -m
set -e

PYTHONUNBUFFERED=1

if [ -z ${PGUSER+x} ]
then
echo "[WARN] PGUSER environment variable not set, defaulting to postgres user 'postgres'"
fi

if [ -z ${PGPASSWORD+x} ]
then
echo "[WARN] PGPASSWORD environment variable not set, defaulting to postgres password 'password'"
fi


echo "This will wipe ALL local Sempo data"

echo "Reset Local Secrets? y/N"
read resetSecretsInput

echo "Persist Ganache? y/N"
read ganachePersistInput

echo "Create Dev Data? (s)mall/(m)edium/(l)arge/(N)o"
read testDataInput

if [ "$testDataInput" == 's' ]; then
    echo "Will create Small Dev Dataset"
    testdata='small'
elif [ "$testDataInput" == 'm' ]; then
    echo "Will create Medium Dev Dataset"
    testdata='medium'
elif [ "$testDataInput" == 'l' ]; then
    echo "Will create Large Dev Dataset"
    testdata='large'
else
    echo "Will not create Dev Dataset"
    testdata='none'
fi

if [ "$resetSecretsInput" == "y" ]; then
  echo ~~~~Creating Secrets
  pushd $SEMPO_PATH/config_files/
  python generate_secrets.py
  popd
fi

MASTER_WALLET_PK=$(awk -F "=" '/master_wallet_private_key/ {print $2}' $SEMPO_PATH/config_files/secret/local_secrets.ini  | tr -d ' ')

echo ~~~~Killing any leftover workers or app
set +e
kill -9 $(ps aux | grep '[r]un.py' | awk '{print $2}')
kill -9 $(ps aux | grep '[c]elery' | awk '{print $2}')

echo ~~~~Resetting readis
redis-server &
redis-cli FLUSHALL

sleep 1

echo ~~~~Resetting postgres
echo If this section hangs, you might have a bunch of idle postgres connections. Kill them using
echo "sudo kill -9 \$(ps aux | grep '[p]ostgres .* idle' | awk '{print \$2}')"

db_server=postgres://${PGUSER:-postgres}:${PGPASSWORD:-password}@localhost:5432
app_db=$db_server/${APP_DB:-grassroots}
eth_worker_db=$db_server/${WORKER_DB:-sempo_eth_worker}

echo ~~~~ App DB $app_db

set -e
psql $db_server -c ''

set +e

psql $db_server -c "DROP DATABASE IF EXISTS ${APP_DB:-grassroots}"
psql $db_server -c "DROP DATABASE IF EXISTS ${WORKER_DB:-sempo_eth_worker}"
psql $db_server -c "CREATE DATABASE ${APP_DB:-grassroots}"
psql $db_server -c "CREATE DATABASE ${WORKER_DB:-sempo_eth_worker}"

pushd $SEMPO_PATH/app
python manage.py db upgrade
popd

pushd $SEMPO_PATH/eth_worker/
alembic upgrade heads
popd

echo ~~~~Resetting Ganache
kill $(ps aux | grep '[g]anache-cli' | awk '{print $2}')

if [ "$ganachePersistInput" == 'y' ]
then
  echo clearing old ganache data
  rm -R $SEMPO_PATH/ganacheDB
  mkdir $SEMPO_PATH/ganacheDB
  ganache-cli -l 80000000 -i 42 --account="${MASTER_WALLET_PK},10000000000000000000000000" --db "$SEMPO_PATH/ganacheDB" &
else
  ganache-cli -l 80000000 -i 42 --account="${MASTER_WALLET_PK},10000000000000000000000000" &
fi

sleep 5

set -e

echo ~~~Starting worker
pushd $SEMPO_PATH/eth_worker
celery -A eth_manager worker --loglevel=INFO --concurrency=8 --pool=eventlet -Q processor,celery,low-priority,high-priority &
sleep 5
popd

echo ~~~Seeding Data
pushd $SEMPO_PATH/app/migrations/
python -u seed.py
popd

echo ~~~Starting App
pushd $SEMPO_PATH/app
python -u run.py &
sleep 10
popd

echo ~~~Creating Default Account
curl 'http://localhost:9000/api/v1/auth/register/'  -H 'Content-Type: application/json' -H 'Origin: http://localhost:9000' --data-binary '{"username":"admin@acme.org","password":"C0rrectH0rse","referral_code":null}' --compressed --insecure
psql $app_db -c 'UPDATE public."user" SET is_activated=TRUE'

echo ~~~Setting up Contracts
python -u devtools/contract_setup_script.py

if [[ "$testdata" != 'none' ]]; then
    echo ~~~Creating test data
    pushd $SEMPO_PATH/app/migrations/
    python -u dev_data.py ${testdata}
    popd
fi

echo ~~~Killing Python Processes
sleep 5
set +e
kill -9 $(ps aux | grep '[r]un.py' | awk '{print $2}')
kill -9 $(ps aux | grep '[c]elery' | awk '{print $2}')
sleep 2

echo ~~~Done Setup! Bringing Ganache to foreground
fg 2
