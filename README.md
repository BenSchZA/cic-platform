# Sarafu CIC Platform

Some of the following commands use Nix. This is optional, but is recommended and will make your life easier.

# Setup

## Submodules

Initialize the git submodules - these will be initialized in the `contrib/` directory.

```bash
git submodule update --init
```

If you're running the pipeline and tests, and want to reset any uncommitted changes in the submodules, run:

```bash
git -C contrib/sempo reset --hard
```

## Patches

This is a work in progress.

To make changes to Sempo submodule files, make a copy of the relevant file in the same subdirectory under `src/sempo`, then create and apply the patch:

```bash
# This will create a `sempo.patch` file
./patches/diff.sh
# This will apply the `sempo.patch` file to the submodule. Do not commit these changes.
./patches/patch.sh
```

For files that need to be removed or replaced entirely in the Sempo submodule, we'll need to give this some more thought - either overriding the PYTHONPATH, or removing the files in a script.

## Environment variables

Create a copy of the `.envrc_example` file. Then fill in the necessary secrets and source the file in your environment:

```bash
cp .envrc_example .envrc
source .envrc
```

To make this easier for yourself, check out `direnv`.

## Nix
> Nix is a powerful package manager for Linux and other Unix systems that makes package management reliable and reproducible. Share your development and build environments across different machines. 

It creates a robust sandboxed environment for software development, but can also just be used for managing system dependencies.

See https://nixos.org/download.html

Installing Nix (required) and Cachix (optional):

```bash
curl -L https://nixos.org/nix/install | sh
nix-env -ibA cachix -f https://cachix.org/api/v1/install
# If you'd like to use a build cache
cachix use sarafu-dev
```

Entering the Nix development environment will make all necessary system dependencies available - this may take a while if you're not using Cachix and if it's your first time entering the shell:

```bash
nix-shell
```

After entering the shell environment, a number of commands will be available to you, such as: `install-deps`, `setup`, `start`. Further instructions will be printed to console.

Within the Nix `shell.nix` you'll see the commands that are run at each step, these could be run manually.

If you'd like to make these dependencies available permanently outside the shell, run: `nix-env -if default.nix`

# Installation

The app requires Redis and Postgres. Please make sure they are running when performing any of the steps in this document.

```bash
install-deps
```

# Development

```bash
setup
```

In another shell, with the previous `setup` command still running:

```bash
start
```

# CI/CD Pipeline

## GitLab

NB: the pipeline, for now, will only run for parent repo branches, not forks. This is for security I guess, and also because it doesn't seem to be possible with GitLab: https://gitlab.com/gitlab-org/gitlab/-/issues/11934

The pipeline runs for PR branches, and for the master branch. The GitLab container registry is used for Docker images. Branch `master` is deployed to staging automatically, whereas production is deployed manually for now, but in future from the `production` branch.

## Local

As much as possible, this will recreate what is run in the GitLab CI/CD job - running tests and building Docker images. GitLab uses `.gitlab-ci.yml` for orchestrating the pipeline, whereas locally we use the files in the `vagrant` directory.

To run the pipeline locally:

```bash
nix-shell
pipeline
```

Please note, this will clone the current repo into a temporary directory `tmp/pipeline` - any uncommitted files won't be included in the pipeline job.

# Dependencies

## System

If you'd rather install your own system dependencies, for whatever system you may have (Linux, macOS, etc.), you can find the dependencies listed in the `default.nix` and `shell.nix` files under `paths` and `buildInputs` respectively.

## Submodules

See [Setup](#-setup) section for initializing git submodules.

* python 3.7.3 (chosen to match bancor dep version)
* ganache-cli 6.7.0 (testing)
* bancor contracts; https://github.com/nolash/contracts (as submodule)
  - same as https://github.com/bancorprotocol/contracts tag v0.5.16, adding convenience bootstrapping scripts and contract migration for ge relevant contracts

Note that deployed sempo bancor contracts were branched on aug 17th 2019, which means that the production app is using 0.4.8 or 0.4.9.

# Vagrant
> Vagrant enables users to create and configure lightweight, reproducible, and portable development environments.

Using Vagrant, the CI/CD pipeline can be recreated for local testing. In future, all testing could be performed in a Vagrant box to be sure that local testing is reproducible.

vagrant up
```bash
vagrant ssh
```

To run the full testing pipeline, after `vagrant ssh`:

```bash
cd ./sarafu
./vagrant/pipeline.sh
```

* Vagrant determines what virtualization provider to use based on your system, this can be overriden, for example: `VAGRANT_DEFAULT_PROVIDER=virtualbox`
* When Vagrant initially starts a Vagrant box, it provisions the environment using the `vagrant/provision.sh` script. This can be run manually using `vagrant provision`.
* The environment is based on the Ubuntu `hashicorp/bionic64` image. A new Vagrant box can be initialized using `vagrant init hashicorp/bionic64`. Note, this will overwrite the existing `Vagrantfile`. 
