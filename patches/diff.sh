#!/usr/bin/env bash

LC_ALL=C diff -aur contrib/sempo src/sempo | grep -v '^Only in' > patches/sempo.patch
# LC_ALL=C diff -aur contrib/sempo/docker-compose.yml docker-compose.yml | grep -v '^Only in' > patches/docker-compose.patch
