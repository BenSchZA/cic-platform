import argparse
import json
import logging

logging.basicConfig(level=logging.DEBUG)
logg = logging.getLogger()

from web3 import Web3

argparser = argparse.ArgumentParser()
argparser.add_argument('--bancor-dir', type=str, dest='bancor_dir', default='contrib/bancor')
argparser.add_argument('--registry-address', type=str, dest='registry_address')
argparser.add_argument('--endpoint', type=str, default='http://localhost:7545')
argparser.add_argument('token_address', type=str)
args = argparser.parse_args()

w3 = Web3(Web3.HTTPProvider(args.endpoint))

contracts = {}
with open('{}/solidity/build/ContractRegistry.abi'.format(args.bancor_dir)) as f:
    json_abi = json.load(f)
    contracts['ContractRegistry'] = w3.eth.contract(
        address=args.registry_address,
        abi=json_abi
        )
    f.close()

converter_registry_address = contracts['ContractRegistry'].get_function_by_name('addressOf')(Web3.toHex(text='BancorConverterRegistry')).call()
logg.debug('Found converter registry {}'.format(converter_registry_address))
with open('{}/solidity/build/BancorConverterRegistry.abi'.format(args.bancor_dir)) as f:
    json_abi = json.load(f)
    contracts['BancorConverterRegistry'] = w3.eth.contract(
        address=converter_registry_address,
        abi=json_abi
        )
    f.close()

smarttoken_json_abi = None
with open('{}/solidity/build/SmartToken.abi'.format(args.bancor_dir)) as f:
    smarttoken_json_abi = json.load(f)
    f.close()

smarttoken_found = False
for c in  contracts['BancorConverterRegistry'].get_function_by_name('getSmartTokens')().call():
    logg.debug('smarttoken {}'.format(c))
    if c == args.token_address:
        smarttoken_found = True
        break

if not smarttoken_found:
    sys.exit(1)

contract_smarttoken = w3.eth.contract(
    address=c,
    abi=smarttoken_json_abi
    )
print(contract_smarttoken.get_function_by_name('owner')().call())
