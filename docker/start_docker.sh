#!/bin/bash

# Author: Louis Holbrook <dev@holbrook.no> https://holbrook.no
# License: GPLv3
# Description: Sets up virtualenv with the correct python interpreter and installs module dependencies for python and node
# 
# extended sempo app invocation used in docker container

. .venv/bin/activate
PYTHONPATH=${SEMPO_OVERRIDE_PATH}:$PYTHONPATH

python ${SEMPO_OVERRIDE_APP_PATH}/run.py 
