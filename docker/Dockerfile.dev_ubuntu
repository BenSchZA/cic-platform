FROM ubuntu:bionic

ENV TZ Africa/Nairobi
ENV DEBIAN_FRONTEND noninteractive

RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN apt-get update && apt-get install -y \
	gnupg \
	git \
	curl \
	jq \
	python3-venv \
	ssh \
	python3-dev \
	gcc \
	openssl \
	libssl-dev \
	libmysqlclient-dev \
	postgresql

# thanks https://stackoverflow.com/questions/25899912/how-to-install-nvm-in-docker
ENV NVM_DIR /usr/local/nvm
ENV NODE_VERSION 10.16.0
ENV NODE_PATH $NVM_DIR/versions/node/v$NODE_VERSION/lib/node_modules
ENV PATH      $NVM_DIR/versions/node/v$NODE_VERSION/bin:$PATH

# Install nvm with node and npm
RUN mkdir -vp $NVM_DIR
RUN curl https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.3/install.sh | bash \
    && . $NVM_DIR/nvm.sh \
    && nvm install $NODE_VERSION \
    && nvm alias default $NODE_VERSION \
    && nvm use default

RUN ls -lR $NVM_DIR
RUN node --version

RUN mkdir -vp /root/.npm
RUN npm install -g ganache-cli
RUN mkdir -p /opt/grassroots


