from eth_keys import keys

class UnsafeWallet:

    def __init__(self, private_key):
        if isinstance(private_key, str):
            private_key = bytearray.fromhex(private_key.replace('0x', ''))
        self.private_key = private_key
        self.address = keys.PrivateKey(private_key).public_key.to_checksum_address() 

