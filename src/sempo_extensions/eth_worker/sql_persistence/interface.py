import config
import logging

logg = logging.getLogger('persistence')

class PersistenceInterface:

    def register_transaction(self, wallet=None):
        new_transaction_id = len(self.transactions_wallets)
        if wallet == None:
            wallet = self.wallet
        self.transactions_wallets.append(wallet)
        self.transactions.append(None)
        logg.debug("added transaction {} {}".format(new_transaction_id, wallet.address))
        return new_transaction_id
   

    def get_transaction_signing_wallet(self, transaction_id):
        return self.transactions_wallets[transaction_id]

 
    def locked_claim_transaction_nonce(self, wallet, transaction_id):
        if self.nonce == -1:
            self.nonce = self.w3.eth.getTransactionCount(wallet.address, block_identifier='pending')
        network_nonce = self.nonce
        self.nonce += 1
        return network_nonce, transaction_id


    def update_transaction_data(self, transaction_id, transaction_data):
        self.transactions[transaction_id] = transaction_data
        logg.info("tx done {} {}".format(transaction_id, transaction_data))


    def get_transaction(self, transaction_id):
        return self.transactions[transaction_id]

    def __init__(self, w3, wallet):
        self.w3 = w3
        self.account = wallet.address
        self.wallet = wallet
        self.transactions = []
        self.transactions_wallets = []
        self.nonce = -1 
