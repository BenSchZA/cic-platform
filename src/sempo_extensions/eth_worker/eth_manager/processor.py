import sys
import logging
import datetime

from eth_keys import keys

import config
from eth_worker.eth_manager.exceptions import PreBlockchainError

logg = logging.getLogger('processor')

# this is copy from SEMPO/eth_worker/eth_manager/processor.py
# is should be included directly when sql_persistence hard coupling is eliminiated
# registry member in constructor has been added
class TransactionProcessor:


    def log_error(self, request, exc, traceback, transaction_id):
        logg.error('{} {}'.format(type(exc).__name__, str(exc.args[0])))


    def process_send_eth_transaction(self, transaction_id,
                                     recipient_address, amount, task_id=None):

        partial_txn_dict = {
            'to': recipient_address,
            'value': amount
        }

        logg.debug('tx to {} amount {}'.format(recipient_address, amount))

        return self.process_transaction(transaction_id, partial_txn_dict=partial_txn_dict, gas_limit=100000)


    def process_transaction(self,
                            transaction_id,
                            unbuilt_transaction=None,
                            partial_txn_dict=None,
                            gas_limit=None,
                            gas_price=None):

        chainId = self.ethereum_chain_id
        gasPrice = gas_price or self.gas_price
        # gasPrice = gas_price or self.get_gas_price()

        signing_wallet_obj = self.persistence_interface.get_transaction_signing_wallet(transaction_id)

        gas = None
        if gas_limit:
            gas = gas_limit
        else:
            try:
                gas = unbuilt_transaction.estimateGas({
                    'from': signing_wallet_obj.address,
                    'gasPrice': gasPrice
                })
            except ValueError as e:
                print("Estimate Gas Failed. Remedy by specifying gas limit.")

                raise e

        nonce, transaction_id = self.persistence_interface.locked_claim_transaction_nonce(
            signing_wallet_obj, transaction_id
        )
        if not gas:
            if gas_limit:
                gas = gas_limit
            else:
                gas = min(int(gas*1.2), 8000000)
        metadata = {
            'gas': hex(int(gas)),
            'gasPrice': gasPrice,
            'nonce': nonce,
        }

        if chainId:
            metadata['chainId'] = chainId

        if unbuilt_transaction:
            txn = unbuilt_transaction.buildTransaction(metadata)
        else:
            txn = {**metadata, **partial_txn_dict}

        signed_txn = self.w3.eth.account.signTransaction(txn, private_key=signing_wallet_obj.private_key)

        try:
            logg.debug('tx {} using nonce {}'.format(transaction_id, nonce))
            result = self.w3.eth.sendRawTransaction(signed_txn.rawTransaction)

        except ValueError as e:

            message = f'Transaction {transaction_id}: {str(e)}'
            #exc = PreBlockchainError(message, False)
            #self.log_error(None, exc, None, transaction_id)
            logg.error('tx fail {}'.format(txn))

            raise PreBlockchainError(message, True)

        # If we've made it this far, the nonce will(?) be consumed
        transaction_data = {
            'hash': signed_txn.hash.hex(),
            'nonce': nonce,
            'submitted_date': str(datetime.datetime.utcnow()),
            'nonce_consumed': True
        }

        logg.info('data for transaction {} {}'.format(transaction_id, transaction_data))

        self.persistence_interface.update_transaction_data(transaction_id, transaction_data)

        return transaction_id

   
    # TODO: args/kwargs should be passed differently middle of arg list
    def process_deploy_contract_transaction(self,
                                            transaction_id,
                                            contract_name,
                                            args=None,
                                            kwargs=None,
                                            gas_limit=None,
                                            task_id=None):

        args = args or tuple()
        if not isinstance(args, (list, tuple)):
            args = [args]

        kwargs = kwargs or dict()

        logg.debug('tx deploy contract {} {}'.format(transaction_id, contract_name))

        contract = self.registry.get_compiled_contract(contract_name)

        constructor = contract.constructor(*args, **kwargs)

        return self.process_transaction(transaction_id, constructor, gas_limit=gas_limit)


    def process_function_transaction(self, transaction_id, contract_address, abi_type,
                                     function_name, args=None, kwargs=None, gas_limit=None, task_id=None):

        args = args or tuple()
        if not isinstance(args, (list, tuple)):
            args = [args]
        args = [self.typecast_argument(a) for a in args]

        kwargs = kwargs or dict()
        kwargs = {k: self.typecast_argument(v) for k, v in kwargs.items()}

        logg.debug('tx function {} {}'.format(abi_type, function_name))

        function = self.registry.get_contract_function(contract_address, function_name, abi_type)

        bound_function = function(*args, **kwargs)

        return self.process_transaction(transaction_id, bound_function, gas_limit=gas_limit)

    def typecast_argument(self, argument):
        if isinstance(argument, dict) and argument.get('type') == 'bytes':
                return argument.get('data').encode()
        return argument
    def process_deploy_contract_transaction(self, transaction_id, contract_name,
                                            args=None, kwargs=None, gas_limit=None, task_id=None):

        args = args or tuple()
        if not isinstance(args, (list, tuple)):
            args = [args]

        kwargs = kwargs or dict()

        logg.debug('tx deploy contract {} {}'.format(transaction_id, contract_name))

        contract = self.registry.get_compiled_contract(contract_name)

        constructor = contract.constructor(*args, **kwargs)

        return self.process_transaction(transaction_id, constructor, gas_limit=gas_limit)


    def wait_for_transaction_receipt(self, transaction_id):
        tx = self.persistence_interface.get_transaction(transaction_id)
        return self.w3.eth.waitForTransactionReceipt(tx['hash'])


    def __init__(self,
                 ethereum_chain_id,
                 w3,
                 gas_price_gwei,
                 gas_limit,
                 persistence_interface,
                 registry,
                 task_max_retries=3):

            self.registry = registry

            self.ethereum_chain_id = int(ethereum_chain_id) if ethereum_chain_id else None
            self.w3 = w3

            self.gas_price = self.w3.toWei(gas_price_gwei, 'gwei')
            self.gas_limit = gas_limit
            self.transaction_max_value = self.gas_price * self.gas_limit

            self.persistence_interface = persistence_interface

            self.task_max_retries = task_max_retries

