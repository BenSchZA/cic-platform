import sys
import json
import config
import logging

from .contract_registry import ContractRegistry as ContractRegistryBase

logg = logging.getLogger('registry')

class ContractRegistry(ContractRegistryBase):
        """Overloads the class in sempo backend eth_worker/eth_manager/contract_registry.py.

        Provides workaround for hardcoded json path
        """

        # TODO: this method should be eliminated, it is included due to dependency in sempo app
        def get_contract_json(self, contract_name):
            with open('{}/solidity/build/contracts/{}.json'.format(self.bancor_dir, contract_name)) as json_file:
                return json.load(json_file)


        def get_contract_abi(self, contract_name):
            return self.contract_abis[contract_name]


        def get_contract_address(self, contract_name):
            return self.contract_addresses[contract_name]


        def register_contract_by_name(self, contract_name, contract_address, abi):
            logg.debug('registering contract {} {}'.format(contract_name, contract_address))
            self.contract_addresses[contract_name] = contract_address
            self.register_abi(contract_name, abi)
            self.register_contract(contract_address, abi)


        def __init__(self, w3, bancor_dir):
            self.bancor_dir = bancor_dir
            self.contracts = {}
            self.contract_addresses = {}
            super(ContractRegistry, self).__init__(w3)
