"""accenture views

Revision ID: 59284ff719bd
Revises: b56b01f25e5e
Create Date: 2020-04-02 23:26:24.972072

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql


# revision identifiers, used by Alembic.
revision = '59284ff719bd'
down_revision = 'b56b01f25e5e'
branch_labels = None
depends_on = None


def upgrade():
    c = op.get_bind()
    try:
        c.execute("""
            create role renderers;
        """)
    except:
        pass


    c.execute("""
        create schema renderers authorization postgres;
        grant usage on schema renderers to renderers;

	create extension postgres_fdw;
	create server r_eth_worker foreign data wrapper postgres_fdw options (host 'localhost', port '5432', dbname 'eth_worker');
	create user mapping for postgres server r_eth_worker options (user 'read_only', password 'fc62200955a8ba8b5f753e720198bc50f096da8eed6a0818');
	create foreign table r_blockchain_transaction ( id int not null, hash varchar, blockchain_task_id int not null, updated timestamp not null ) server r_eth_worker options (schema_name 'public', table_name 'blockchain_transaction');
	create foreign table r_blockchain_task ( id int not null, uuid varchar, status_text varchar ) server r_eth_worker options (schema_name 'public', table_name 'blockchain_task');



        create or replace view renderers.tmp_user_gender_view as 
		select 
			user_id,
			case 
				when c.value ~* 'female' then 'f'
				when c.value ~* 'male' then 'm'
				else 'o'
				end as gender
			from custom_attribute_user_storage c
			where name = 'gender';



        create or replace view renderers.tmp_user_poa_wallet_view as
		select 
			user_id,
			c.value as blockchain_address
		from
			custom_attribute_user_storage c
			where c.name = 'GE_wallet_address';



        create or replace view renderers.transfer_view as
		select 
			c.recipient_user_id as recipient_user_id,
			nullif(c.transfer_use->>0, '') as business_type
		from 	
			credit_transfer c;



        create or replace function renderers.confidence(user_id int) returns float as $$
		declare
			registered_uses numeric;
			all_uses numeric;
		begin
			select into registered_uses
				count(*)
				from renderers.user_meta_view u
					inner join renderers.transfer_view t on t.recipient_user_id = u.id
				where u.id = user_id
					and u.business_type = t.business_type;

			if registered_uses = 0 then
				return 0;
			end if;

			select into all_uses
				count(*)
				from renderers.transfer_view t
				where t.recipient_user_id = user_id;

			if all_uses = 0 then
				return 0;
			else
				return (registered_uses / all_uses);
			end if;

		end;
		$$
		language plpgsql;



        create or replace view renderers.token_view as
            select 
                    t.id,
                    t.name,
                    t.symbol,
                    t._decimals as decimals,
                    t.token_type as type,
                    t.address as address,
                    json_extract_path_text(subexchange_address_mapping, t.address, 'subexchange_address') as converter_address
            from token t
                    left join exchange_contract_token_association_table a on a.token_id = t.id,
                    exchange_contract e
            where
                    a.token_id is null or e.id = a.exchange_contract_id;

            grant select on token to renderers;


        create or replace view renderers.user_meta_view as
            select
                    u.id,
                    u.created,
                    g.gender as gender,
                    u.lat || ',' || u.lng as location,
                    u._held_roles as roles,
                    u.primary_blockchain_address as current_blockchain_address,
                    w.blockchain_address as previous_blockchain_address,
                    t._name as business_type,
                    trunc(a._balance_wei * (10 ^ (-18))) as bal,
                    min(c.created) as start,
                    max(c.created) as last_send
            from
                    public.user u left join public.transfer_usage t on t.id = u.business_usage_id
                            left join credit_transfer c on c.sender_user_id = u.id
                            left join transfer_account a on a.id = u.default_transfer_account_id
                            left join renderers.tmp_user_poa_wallet_view w on u.id = w.user_id
                            left join renderers.tmp_user_gender_view g on u.id = g.user_id
                    group by u.id, business_type, g.gender, a._balance_wei, w.blockchain_address;



        create or replace view renderers.tx_meta_view as
            select	
                    c.id,
                    c.created as timeset,
                    t.name as token_name,
                    a_s.blockchain_address as source,
                    a_r.blockchain_address as target,
                    trunc(c._transfer_amount_wei * (10 ^ -18)) as weight,
                    c.transfer_subtype as transfer_subtype,
                    c.transfer_status as transfer_status,
                    nullif(c.transfer_use->>0, '') as transfer_use,
                    c.blockchain_task_uuid as task_uuid
            from
                    credit_transfer c inner join token t on c.token_id = t.id
                            inner join transfer_account a_s on c.sender_transfer_account_id = a_s.id
                            inner join transfer_account a_r on c.recipient_transfer_account_id = a_r.id;



        create or replace view renderers.eth_view as
            select
                    r_task.uuid as uuid,
                    r_tx.updated as updated,
                    r_tx.hash as tx_hash
            from 
                    r_blockchain_transaction r_tx
                            inner join  r_blockchain_task r_task on r_tx.blockchain_task_id = r_task.id
                    where r_task.status_text = 'SUCCESS';

	    grant select on all tables in schema renderers to renderers;
            grant select on all tables in schema renderers to read_only;
    """)
    pass


def downgrade():
    c = op.get_bind()
    c.execute("""
        drop view renderers.eth_view;
        drop view renderers.tx_meta_view;
        drop view renderers.user_meta_view;
        drop view renderers.token_view;
        drop function renderers.confidence;
        drop view renderers.transfer_view;
        drop view renderers.tmp_user_poa_wallet_view;
        drop view renderers.tmp_user_gender_view;
        drop foreign table r_blockchain_task;
	drop foreign table r_blockchain_transaction;
	drop user mapping for postgres server r_eth_worker;
	drop server r_eth_worker;
	drop extension postgres_fdw;
        drop schema renderers;

    """)
    pass
