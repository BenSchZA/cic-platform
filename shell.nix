with import <nixpkgs> { };

# To use a binary cache, install Cachix:
# nix-env -ibA cachix -f https://cachix.org/api/v1/install
# cachix use sarafu-dev
# To update the binary cache:
# nix-build default.nix | cachix push sarafu-dev

let
  commitRev = "025deb80b2412e5b7e88ea1de631d1bd65af1840";
  nixpkgs = builtins.fetchTarball {
    url = "https://github.com/NixOS/nixpkgs-channels/archive/${commitRev}.tar.gz";
    sha256 = "09mp6vqs0h71g27w004yrz1jxla31ihf18phw69wj61ix74ac4m0";
  };
  pkgs = import nixpkgs { config = {}; };
  python = pkgs.python36;
  pythonPackages = pkgs.python36Packages;
  script-install = pkgs.writeShellScriptBin "install-deps" ''
    virtualenv venv
    source venv/bin/activate
    pushd $SEMPO_PATH/app
    python -m pip install -r slow_requirements.txt
    python -m pip install -r requirements.txt
    popd
    pushd $SEMPO_PATH/eth_worker
    python -m pip install -r requirements.txt
    popd
    pushd $SEMPO_PATH/worker
    python -m pip install -r requirements.txt
    popd
    pushd $SEMPO_PATH/test
    python -m pip install -r requirements.txt
    popd
    pushd $SEMPO_PATH/app
    npm install
    npm run-script build
    popd
  '';
  script-setup = pkgs.writeShellScriptBin "setup" ''
    source venv/bin/activate
    bash devtools/quick_setup_script.sh
  '';
  script-start = pkgs.writeShellScriptBin "start" ''
    source venv/bin/activate
    python -u $SEMPO_PATH/app/run.py
  '';
  script-pipeline = pkgs.writeShellScriptBin "pipeline" ''
    ./vagrant/pipeline.sh
  '';
in
stdenv.mkDerivation {
  name = "dev-shell";
  buildInputs = [ (import ./default.nix { inherit pkgs; }) ] ++ [
    python
    pythonPackages.pip
    pythonPackages.setuptools
    pythonPackages.virtualenvwrapper
  ] ++ [
    script-install
    script-setup
    script-start
  ];
  shellHook = "
    source .envrc
    unset SOURCE_DATE_EPOCH
    NIX_PYTHONPATH=${pythonPackages.mysqlclient}/lib/python3.6/site-packages:${pythonPackages.pandas.outPath}/lib/python3.6/site-packages:${pythonPackages.numpy.outPath}/lib/python3.6/site-packages
    export PYTHONPATH=$NIX_PYTHONPATH:$SEMPO_PATH:`pwd`/venv/${python.sitePackages}/:$PYTHONPATH
    export LD_LIBRARY_PATH=${stdenv.cc.cc.lib}/lib/libstdc++.so.6

    echo 'To install development environment Python dependencies, run: install-deps'
    echo 'To setup & seed development environment, run: setup'
    echo 'To start app after setup, run: start'
    echo 'App host: localhost:9000'
    echo 'Username and password: admin@acme.org / C0rrectH0rse'
  ";
}
