#!/usr/bin/env nix-shell
#!nix-shell --pure -i bash --keep SEMPO_PATH --keep DOCKER_HOST --keep GIT_SSL_CAINFO --keep DOCKER_DRIVER --keep AWS_ACCESS_KEY_ID --keep AWS_SECRET_ACCESS_KEY --keep AWS_REGION ../shell.nix

set -o errexit -o nounset -o pipefail

FILE=.envrc
if [ -f "$FILE" ]; then
    echo "Sourcing $FILE"
    source .envrc
else 
    echo "$FILE does not exist."
fi

unset APP_DB
export CONTAINER_MODE=TEST
export SEMPO_PATH=contrib/sempo

alias python=python3
alias pip=pip3

[ -d './tmp/pipeline' ] && rm -Rf ./tmp/pipeline
mkdir -p ./tmp/pipeline
git clone . ./tmp/pipeline

pushd ./tmp/pipeline
git submodule update --init
./patches/patch.sh

./vagrant/generate_secrets.sh
./vagrant/node_install.sh
./vagrant/node_build.sh
./vagrant/docker_build.sh
./vagrant/test_backend.sh
./vagrant/test_frontend.sh
popd

[ -d './tmp/pipeline' ] && rm -Rf ./tmp/pipeline
