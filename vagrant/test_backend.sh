#!/usr/bin/env bash

docker-compose up --force-recreate --exit-code-from app
