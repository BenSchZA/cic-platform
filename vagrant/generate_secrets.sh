#!/usr/bin/env bash

pushd $SEMPO_PATH
[ ! -d config_files/secret ] && mkdir config_files/secret
pushd config_files
python3 generate_secrets.py -n docker_test
popd
popd
