# TODO: needs documentation

import time
import sys
import random
from uuid import uuid4

import logging
logging.basicConfig(level=logging.DEBUG)
logg = logging.getLogger()

from server import create_app, db, bt
from server.models.user import User
from server.models.organisation import Organisation
from server.models.token import Token
from server.models.credit_transfer import CreditTransfer
from server.models.transfer_account import TransferAccount, TransferAccountType
from server.models.transfer_usage import TransferUsage
from server.models.exchange import ExchangeContract, Exchange
from server.utils.user import create_transfer_account_user
from server.utils.transfer_enums import TransferSubTypeEnum, TransferTypeEnum
from server.utils.credit_transfer import make_blockchain_transfer

from flask import g

SEEDED_RESERVE_BALANCE = 1000000000000000000000
RESERVE_LIQUID_MULTIPLIER = 4


def create_transfer(amount, sender_user, recipient_user, token, subtype=None):
    logg.debug('creating transfer {} => {} = {}:{}'.format(sender_user.primary_blockchain_address, recipient_user, token, amount))
    transfer = CreditTransfer(
        amount=amount,
        sender_user=sender_user,
        recipient_user=recipient_user,
        token=token,
        uuid=str(uuid4()))

    db.session.add(transfer)

    transfer.resolve_as_completed()

    transfer.transfer_type = TransferTypeEnum.PAYMENT
    transfer.transfer_subtype = subtype

    return transfer


def create_transfers(user_list, wanted_nr_transfers, token):
    transfer_list = []
    i = 0
    while i < wanted_nr_transfers:
        try:
            shuffled = user_list.copy()
            random.shuffle(shuffled)

            transfer = create_transfer(
                amount=random.randint(1, 5),
                sender_user=shuffled[0],
                recipient_user=shuffled[1],
                token=token
            )

            transfer_list.append(transfer)
            i += 1
        except:
            pass
    return transfer_list


def seed_transfers(user_list, admin_user, token):
    print('Disbursing to users')
    for user in user_list:
        create_transfer(
            amount=50,
            sender_user=admin_user,
            recipient_user=user,
            token=token,
            subtype=TransferSubTypeEnum.DISBURSEMENT
        )

    time.sleep(5)
    number_of_transfers = 30
    print('Creating %d transactions' % number_of_transfers)
    transfers = create_transfers(user_list, number_of_transfers, token)
    for t in transfers:
        logg.debug('received for blockchain dispatch transfer {}'.format(t))


def get_or_create_transfer_user(email, business_usage, organisation):

    user = User.query.filter_by(
        email=str(email).lower()).first()
    if user:
        return user
    else:
        first_name = random.choice(['Magnificent', 'Questionable', 'Bold', 'Hungry', 'Trustworthy', 'Valued', 'Free',
                                    'Obtuse', 'Frequentist', 'Long', 'Sinister', 'Happy', 'Safe', 'Open', 'Cool'])
        last_name = random.choice(['Panda', 'Birb', 'Doggo', 'Otter', 'Swearwolf', 'Kitty', 'Lion', 'Chimp', 'Cthulhu'])
        is_beneficiary = random.choice([True, False])

        phone = ''.join([str(random.randint(0,10)) for i in range(0, 10)])

        user = create_transfer_account_user(
            first_name=first_name,
            last_name=last_name,
            email=email,
            phone='+49'+phone,
            organisation=organisation,
            is_beneficiary=is_beneficiary,
            is_vendor=not is_beneficiary
        )

        user.business_usage = business_usage

        return user


def create_users_different_transfer_usage(wanted_nr_users, new_organisation):
    i = 1
    user_list = []
    transer_usages = TransferUsage.query.all()
    while i < wanted_nr_users:
        random_usage = random.choice(transer_usages)

        new_user = get_or_create_transfer_user(
            email=f'user-nr-{i}@test.com',
            business_usage=random_usage,
            organisation=new_organisation,
        )

        user_list.append(new_user)
        i += 1
    return user_list


def make_exchange(user, from_token, to_token, from_amount):
    exchange = Exchange()

    exchange.exchange_from_amount(
        user=user,
        from_token=from_token,
        to_token=to_token,
        from_amount=from_amount
    )

    db.session.add(exchange)


app = create_app()
with app.app_context() as ctx:
    ctx.push()

    g.authorizing_user_id = 1

    reserve_token = db.session.query(Token).filter(
        Token.token_type == 'RESERVE'
        ).first()
    logg.info('reserve token {}'.format(reserve_token))

    smart_token = db.session.query(Token).filter(
        Token.token_type == 'LIQUID'
        ).first()
    logg.info('smart token {}'.format(smart_token))


    admin_org = Organisation.master_organisation()
    logg.info('admin org {}'.format(admin_org))


    # for now the transfer_account is not bound to the organisation
    # the method should also update any "admin" users associated with the organisations
    admin_org.bind_token(reserve_token)
    db.session.add(admin_org)
    db.session.commit()
    db.session.flush()

    admin_org = Organisation.master_organisation()
    for t in admin_org.transfer_accounts:
        if t.token.id == reserve_token.id and t.account_type == TransferAccountType.ORGANISATION:
            t._balance_wei = SEEDED_RESERVE_BALANCE
            db.session.add(t)
            db.session.commit()
            db.session.flush()
            break


    # this organisation will have an automatically create wallet
    new_org = db.session.query(Organisation).filter(Organisation.external_auth_username=='admin_grassroots_economics').first()
    new_user = User()
    db.session.add(new_user)
    db.session.commit()


    admin_user = db.session.query(User).execution_options(show_all=True).get(1)
    admin_user.add_user_to_organisation(admin_org, is_admin=True)
    db.session.add(admin_user)
    db.session.commit()
    db.session.flush()

    admin_user = db.session.query(User).execution_options(show_all=True).get(1)
    logg.info('admin reload user {} {}'.format(admin_user, admin_user.organisations))
    for t in admin_user.transfer_accounts:
        logg.debug('list transfer account (org); user {} transfer account {}'.format(admin_user, t))


    admin_user = db.session.query(User).execution_options(show_all=True).get(1)
    transfer_account_reserve_user = TransferAccount(
            name='admin user reserve',
            blockchain_address=admin_user.primary_blockchain_address,
            bound_entity=admin_user,
            account_type=TransferAccountType.USER,
            _balance_wei=SEEDED_RESERVE_BALANCE,
            )
    db.session.add(transfer_account_reserve_user)
    transfer_account_liquid_user = TransferAccount(
            name='admin user liquid',
            blockchain_address=admin_user.primary_blockchain_address,
            bound_entity=admin_user,
            account_type=TransferAccountType.USER,
            _balance_wei=4000000000000000000000,
            token=smart_token,
            )
    db.session.add(transfer_account_liquid_user)
    
    for t in admin_user.transfer_accounts:
        logg.debug('list transfer account (user); user {} transfer account {}'.format(admin_user, t))


    disbursement_user = User()
    disbursement_user.create_admin_auth('disbursement@example.com', 'barfoo---', 'superadmin', new_org) 
    tfaurl = disbursement_user.tfa_url
    logg.info('tfa url for disbursement user: {}'.format(tfaurl))
    disbursement_user.is_activated = True
    disbursement_user.TFA_enabled = True
    db.session.add(disbursement_user)
    transfer = CreditTransfer(
        amount=4000,
        sender_user=admin_user,
        recipient_user=disbursement_user,
        token=smart_token,
        uuid=str(uuid4()))
    db.session.add(transfer)
    transfer.resolve_as_completed()
    transfer.transfer_type = TransferTypeEnum.PAYMENT

    logg.info('Create a list of users with a different business usage id')
    user_list = create_users_different_transfer_usage(15, new_org)

    
    logg.info('Making Bulk Transfers')
    seed_transfers(user_list, admin_user, smart_token)
#    db.session.commit()
#    db.session.flush()

#    logg.info('Making exchanges')
#    make_exchange(
#        user=admin_user,
#        from_token=reserve_token,
#        to_token=smart_token,
#        from_amount=2
#    )
#
#    create_transfer(
#        amount=10,
#        sender_user=admin_user,
#        recipient_user=user_list[0],
#        token=reserve_token
#    )

#    make_exchange(
#        user=user_list[0],
#        from_token=reserve_token,
#        to_token=smart_token,
#        from_amount=10
#    )
#
#    create_transfer(
#        amount=2,
#        sender_user=user_list[0],
#        recipient_user=user_list[1],
#        token=smart_token
#    )

    db.session.commit()
    ctx.pop()


