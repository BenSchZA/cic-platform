from urllib import request
import urllib
import logging
import json

import pyotp

from client_error import ClientAuthError

logg = logging.getLogger(__name__)

OTP_EXPIRY_INTERVAL = 1

class ApiClient:

    def __init__(self, host='localhost', port=9000, use_ssl=True, otp_secret=None):
        logg.debug('setting up api client {} {} ssl: {}'.format(host, port, use_ssl))
        self.host = host
        self.port = port
        self.proto = 'http'
        if use_ssl:
            self.proto += 's'
        self.api_url_core = self.proto + '://' + self.host + ':' + str(self.port) + '/api/v{}'
        self.otp = otp_secret
        self.auth_token = None
        self.tfa_token = None
        self.user_id = -1



    def api_url(self, version=1):
        return self.api_url_core.format(version)



    def token(self):
        tkn = self.auth_token
        if self.tfa_token != None:
            tkn += '|' + self.tfa_token
        return tkn



    def tfa(self):
        totp = pyotp.TOTP(self.otp).now()

        url = self.api_url() + '/auth/tfa/'
        req = urllib.request.Request(url)
        data = {
            'otp': totp,
            'otp_expiry_interval': OTP_EXPIRY_INTERVAL,
            }
        data_str = json.dumps(data)
        data_bytes = data_str.encode('utf-8')
        req.add_header('Content-Type', 'application/json')
        req.add_header('Accept', 'application/json')
        req.add_header('Authorization', self.auth_token)
        req.data = data_bytes

        #logg.debug('sending tfa request {} {}'.format(url, data_str))
        logg.debug('sending tfa request {}'.format(url))
        response = urllib.request.urlopen(req)
        response_json = json.loads(response.read())
        logg.debug('response tfa {}'.format(response_json))
        self.tfa_token = response_json['tfa_auth_token']
        self.user_id = response_json['user_id']
        logg.info('logged in as user id {}'.format(self.user_id))



    def authorize(self, email, password):
        url = self.api_url() + '/auth/request_api_token/'
        req = urllib.request.Request(url)
        data = {
            'email':    email,
            'password': password,
            }
        #logg.debug('urllib {} {} {}'.format(urllib.__file__, url, data))
        data_str = json.dumps(data)
        data_bytes = data_str.encode('utf-8')
        req.add_header('Content-Type', 'application/json')
        req.add_header('Accept', 'application/json')
        req.data = data_bytes

        #logg.debug('sending api token request {} {}'.format(url, data_str))
        logg.debug('sending api token request {}'.format(url)) #, data_str))
        status = 0
        token = None
        try:
            urllib.request.urlopen(req)
        except urllib.error.HTTPError as e:
            if e.code != 401:
                raise(e)

            response_json = json.load(e)
            logg.debug('response authorize {}'.format(response_json))
            tfa_failure = response_json['tfa_failure']
            self.auth_token = response_json['auth_token']
            if tfa_failure:
                if self.otp == None:
                    raise ClientAuthError('TFA needed but opt missing')
                self.tfa()
                
        logg.debug('{}|{}'.format(self.auth_token, self.tfa_token))



    def get(self, endpoint, version=1):

        url = self.api_url(version) + endpoint
        req = urllib.request.Request(url)
        req.add_header('Accept', 'application/json')
        if self.auth_token != None:
            req.add_header('Authorization', self.token())

        logg.debug('sending get {}'.format(url))
        try:
            response = urllib.request.urlopen(req)
        except urllib.error.HTTPError as e:
            logg.error('get error: {} {}'.format(e.code, e.msg))
            raise(e)
        response_json = json.loads(response.read())
        logg.debug('response {}'.format(response_json))
        return response_json



    # TODO: data must be dict, check that it is
    def put(self, endpoint, data, version=1):

        url = self.api_url(version) + endpoint
        
        req = urllib.request.Request(url, method='PUT')
        req.add_header('Content-Type', 'application/json')
        req.add_header('Accept', 'application/json')
        if self.auth_token != None:
            req.add_header('Authorization', self.token())
        data_str = json.dumps(data)
        data_bytes = data_str.encode('utf-8')
        req.data = data_bytes

        logg.debug('sending put {} {}'.format(url, data_str))
        response = urllib.request.urlopen(req)
        response_json = json.loads(response.read())
        logg.debug('response {}'.format(response_json))
        return response_json



    # TODO: data must be dict, check that it is
    def post(self, endpoint, data, version=1):

        url = self.api_url(version) + endpoint
        
        req = urllib.request.Request(url, method='POST')
        req.add_header('Content-Type', 'application/json')
        req.add_header('Accept', 'application/json')
        if self.auth_token != None:
            req.add_header('Authorization', self.token())
        data_str = json.dumps(data)
        data_bytes = data_str.encode('utf-8')
        req.data = data_bytes

        logg.debug('sending post {} {}'.format(url, data_str))
        try:
            response = urllib.request.urlopen(req)
        except urllib.error.HTTPError as e:
            logg.error('get error: {} {}'.format(e.code, e.read()))
            raise(e)
        response_json = json.loads(response.read())
        logg.debug('response {}'.format(response_json))
        return response_json

