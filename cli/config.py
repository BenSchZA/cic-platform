#!/usr/bin/python3

import sys
import os

import configparser
import logging

logg = logging.getLogger('config')

import gnupg

# default values for minimal required sempo app config directives
REDIS_URL = 'redis://localhost:6379'
IS_USING_BITCOIN = False
ETH_CHAIN_ID = 42
ETH_HTTP_PROVIDER = 'http://localhost:7545'
ETH_WEBSOCKET_PROVIDER = 'http://localhost:7545'
ETH_GAS_PRICE = 2
ETH_GAS_LIMIT = 800000000
ETH_CHECK_TRANSACTION_RETRIES = 1
ETH_CHECK_TRANSACTION_RETRIES_TIME_LIMIT = 10
ETH_CHECK_TRANSACTION_BASE_TIME = 120
ETH_CONTRACT_TYPE = "ccv"
ETH_CONTRACT_ADDRESS = None
MASTER_WALLET_PRIVATE_KEY = None
WITHDRAW_TO_ADDRESS = None
SYNCRONOUS_TASK_TIMEOUT = 3 # sic
CURRENCY_DECIMALS = 18

# added to sempo by ge branching
IS_USING_REKOGNITION = False
IS_USING_GEOLOCATION = False

# local use only
CONFIG_FILE = './config.conf'
BANCOR_DIR = '../contrib/bancor'
SEMPO_DIR = '../contrib/sempo'
WALLET_DIR = './keystore'
DEFAULT_ACCOUNT = None
RESERVE_RATIO_PPM = 250000
ZERO_ADDRESS = '0x0000000000000000000000000000000000000000'
ETH_CONTRACT_CONTRACTREGISTRY_ADDRESS = None

# client user only
CLIENT_OTP_SECRET = None
CLIENT_EMAIL = None
CLIENT_PASSWORD = None
CLIENT_DEFAULT_ORGANISATION = 1

# USSD related
USSD_USER = None
USSD_PASS = None
USSD_SERVICE_CODE = None

sys.path.append(os.path.abspath('../src/sempo_extensions/eth_worker'))

# translates sempo config FILE directives to sempo app config directives
_config_translate_dict = {
    'ETHEREUM.contract_address':            'ETH_CONTRACT_ADDRESS',
    'ETHEREUM.withdraw_to_address':         'WITHDRAW_TO_ADDRESS',
    'ETHEREUM.master_wallet_private_key':   'MASTER_WALLET_PRIVATE_KEY',
    'ETHEREUM.default_account':             'DEFAULT_ACCOUNT',
    'ETHEREUM.http_provider':               'ETH_HTTP_PROVIDER',
    'ETHEREUM.websocket_provider':          'ETH_WEBSOCKET_PROVIDER',
    'ETHEREUM.gas_price_gwei':              'ETH_GAS_PRICE',
    'ETHEREUM.gas_limit':                   'ETH_GAS_LIMIT',
    'ETHEREUM.chain_id':                    'ETH_CHAIN_ID',
    'ETHEREUM.registry_address':            'ETH_CONTRACT_CONTRACTREGISTRY_ADDRESS',
    'CLIENT.otp_secret':                    'CLIENT_OTP_SECRET',
    'CLIENT.email':                         'CLIENT_EMAIL',
    'CLIENT.password':                      'CLIENT_PASSWORD',
    'CLIENT.default_organisation':          'CLIENT_DEFAULT_ORGANISATION',
    'USSD.user':                            'USSD_USER',
    'USSD.pass':                            'USSD_PASS',
    'USSD.service_code':                    'USSD_SERVICE_CODE',
}        

logg.debug("python path {}".format(sys.path))

class Decrypter():

    def __init__(self):
        self.gpg = None


    def decrypt(self, k, entry):

        if entry[:5] == '!gpg(':
            filename = entry[5:len(entry)-1]
            if filename == '':
                filename = '{}.asc'.format(k)
            with open(filename, 'rb') as f:
                if self.gpg == None:
                    logging.getLogger('gnupg').setLevel(logging.ERROR)
                    self.gpg = gnupg.GPG(use_agent=True)
                    self.gpg.encoding = 'utf-8'
                c = f.read(-1)
                f.close()
                logg.debug('decrypting key {}'.format(k))
                d = self.gpg.decrypt(c)
                if not d.ok:
                    raise ValueError('cannot decrypt: {}'.format(d.status))
                return str(d)

        return entry


def merge_from_file(filename):
    '''loads configuration directives from file and updates any valid values found in the _config_translate_dict translation table
    '''
    g = globals()
    cp = configparser.ConfigParser()
    logg.debug('config merge from ' + filename)
    cp.read(filename)
    dec = Decrypter()
    for k in cp.keys():
        for kk in cp[k].keys():
            kkk = '.'.join([k, kk])
            if kkk in _config_translate_dict:
                v = cp[k][kk]
                kkkk = _config_translate_dict[kkk]
                g[kkkk] = dec.decrypt(kk, v)
                logg.info('set config directive ' + kkkk) # + ' = ' + v)
            else:
                logg.debug('ignore config directive ' + k + ':' + kk)
