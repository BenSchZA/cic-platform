import logging

logg = logging.getLogger(__name__)

def process_args(argparser):
    argparser.add_argument('from_user_id', help='user id of user to reclaim from')
    argparser.add_argument('amount', help='amount of tokens to reclaim')


def validate_args(args):
    pass


def execute(client, config, eargs):

    amount = int(eargs.amount)
    if amount < 1:
        raise ValueError('amount cannot be 0 or less')

    r = client.get('/user/{}/?org={}'.format(eargs.from_user_id, eargs.o))
    transfer_accounts = r['data']['user']['transfer_accounts']
    transfer_account_count = len(transfer_accounts)
    if transfer_account_count > 1:
        logg.warning('user id {} has {} transfer accounts'.format(eargs.from_user_id, transfer_account_count))

    transfer_account_id = transfer_accounts[0]['id']
    logg.debug('have transfer account id {} for user id {}'.format(transfer_account_id, eargs.from_user_id))

    balance = int(transfer_accounts[0]['balance'])
    logg.debug('transfer account id {} has a balance of {}'.format(transfer_accounts[0]['id'], amount))
    if amount > balance:
        raise ValueError('amount {} is higher than user balance {}̈́'.format(amount, balance))

    r = client.get('/me/transfer_account/')
    logg.debug('response {}'.format(r))
    me_transfer_accounts = r['data']['transfer_accounts']
    me_transfer_account_count = len(transfer_accounts)
    if me_transfer_account_count > 1:
        logg.error('recipient user {} has {} transfer accounts'.format(client.user_id, me_transfer_account_count))
        return False
    me_transfer_account_id = me_transfer_accounts[0]['id']

    # NOTE: in the api "transfer_type" gets translated to db model column "transfer_subtype" when it has certain values
    data = {
        "recipient_transfer_account_id": me_transfer_account_id,
        "sender_transfer_account_id": transfer_account_id,
        "transfer_amount": int(amount),
        "target_balance": None,
        "transfer_type": "RECLAMATION",
        "org": eargs.o,
        "token_id": 2,
            }
    r = client.post('/credit_transfer/', data)
    return True
