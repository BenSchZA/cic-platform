import json
import config


def is_read_only(args=None):
    return False



def process_args(argparser):
    argparser.add_argument('--beneficiary', type=str, dest='to', help='beneficiary address (default: sender)')
    argparser.add_argument('--name', type=str, dest='token_name', help='token name')
    argparser.add_argument('--symbol', type=str, dest='token_symbol', help='token symbol')
    argparser.add_argument('--reserve-ratio', type=int, dest='reserve_ratio', default=config.RESERVE_RATIO_PPM, help='reserve to token ratio (default: {})'.format(config.RESERVE_RATIO_PPM), nargs='?')
    argparser.add_argument('reserve_token_amount', type=int, help='amount of reserve tokens to send to converter')



def execute(session, eargs):

    assert(eargs.reserve_ratio <= 1000000)


    # process args
    token_symbol = eargs.token_symbol
    token_name = eargs.token_name
    if token_name == None:
        token_name = token_symbol
    reserve_amount = eargs.reserve_token_amount
    smart_amount = (reserve_amount*1000000)//eargs.reserve_ratio

    beneficiary_address = session.wallet.address
    if eargs.to != None:
        beneficiary_address = session.w3.toChecksumAddress(eargs.to)

    session.logger.info('smart token {} {}: reserve {} x {} = token {}'.format(token_name, token_symbol, reserve_amount, eargs.reserve_ratio/10000, smart_amount))


    # prepare contract data
    converter_factory_address = session.registry.get_contract_address('BancorConverterFactory')
    reserve_address = session.registry.get_contract_address('EtherToken')
    registry_address = session.registry.get_contract_address('ContractRegistry')
    converter_registry_address = session.registry.get_contract_address('BancorConverterRegistry')


    # deploy smart token contract
    # TODO: translate contract names to use same identifies as in get_contract_address
    # TODO: check smart token registry to avoid duplicate names and symbols
    txid = session.persistence.register_transaction()
    session.processor.process_deploy_contract_transaction(txid, "SmartToken", [token_name, token_symbol, config.CURRENCY_DECIMALS])
    hsh = session.persistence.transactions[txid]['hash']
    rcpt = session.processor.wait_for_transaction_receipt(txid)
    smart_token_address = session.w3.toChecksumAddress(rcpt.contractAddress)
    session.logger.info('deployed smart token contract {} {}'.format(hsh, smart_token_address))


    # deploy converter for smart token against reserve token
    args = [smart_token_address, registry_address, 0, reserve_address,  eargs.reserve_ratio]
    txid = session.persistence.register_transaction()
    session.processor.process_function_transaction(txid, converter_factory_address, 'BancorConverterFactory', 'createConverter', args=args, gas_limit=config.ETH_GAS_LIMIT)
    hsh = session.persistence.transactions[txid]['hash']
    rcpt = session.processor.wait_for_transaction_receipt(txid)
    converter_factory_contract = session.registry.get_contract_by_address(converter_factory_address)
    ev = converter_factory_contract.events.NewConverter().processReceipt(rcpt)
    session.logger.debug('{}'.format(rcpt))
    converter_address = session.w3.toChecksumAddress(ev[0].args['_converter'])
    registry_converter_id = 'bancor_converter_{}'.format(token_symbol)
    converter_abi = session.registry.get_contract_abi('BancorConverter')
    session.registry.register_contract_by_name(registry_converter_id, converter_address, converter_abi)
    session.logger.info('deployed converter contract {} {}'.format(hsh, registry_converter_id))


    # accept ownership of tx sender to converter
    args = []
    session.logger.info('converter accept ownership of smart token')
    txid = session.persistence.register_transaction()
    session.processor.process_function_transaction(txid, converter_address, registry_converter_id, 'acceptOwnership', args=args, gas_limit=config.ETH_GAS_LIMIT)


    # transfer reserve tokens to converter
    args = [converter_address, 0]
    session.logger.info('approving tx reserve tokens {} to {}'.format(args[1], args[0]))
    txid = session.persistence.register_transaction()
    session.processor.process_function_transaction(txid, reserve_address, 'EtherToken', 'approve', args=args, gas_limit=config.ETH_GAS_LIMIT)
    args = [converter_address, reserve_amount]
    txid = session.persistence.register_transaction()
    session.processor.process_function_transaction(txid, reserve_address, 'EtherToken', 'approve', args=args, gas_limit=config.ETH_GAS_LIMIT)
    session.logger.info('transfer tx reserve tokens {} to {}'.format(args[1], args[0]))
    txid = session.persistence.register_transaction()
    session.processor.process_function_transaction(txid, reserve_address, 'EtherToken', 'transfer', args=args, gas_limit=config.ETH_GAS_LIMIT)


    # issue smart tokens on reserve
    args = [beneficiary_address, smart_amount]
    session.logger.info('issue {}'.format(args))
    txid = session.persistence.register_transaction()
    session.processor.process_function_transaction(txid, smart_token_address, 'SmartToken', 'issue', args=args, gas_limit=config.ETH_GAS_LIMIT)


    # transfer ownership of smart token to converter
    args = [converter_address]
    session.logger.info('transfer ownership of smart token to converter')
    txid = session.persistence.register_transaction()
    session.processor.process_function_transaction(txid, smart_token_address, 'SmartToken', 'transferOwnership', args=args, gas_limit=config.ETH_GAS_LIMIT)

    args = []
    session.logger.info('converter accept ownership of smart token')
    txid = session.persistence.register_transaction()
    session.processor.process_function_transaction(txid, converter_address, registry_converter_id, 'acceptTokenOwnership', args=args, gas_limit=config.ETH_GAS_LIMIT)

    args = [converter_address]
    session.logger.info('add converter to converter registry')
    txid = session.persistence.register_transaction()
    session.processor.process_function_transaction(txid, converter_registry_address, 'BancorConverterRegistry', 'addConverter', args=args, gas_limit=config.ETH_GAS_LIMIT)
   
    print(smart_token_address)
    return
