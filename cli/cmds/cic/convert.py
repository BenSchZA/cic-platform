import config


def is_read_only(eargs):
    return eargs.d



# TODO: source token should be automagically determined by address, symbol, name (latter two in that order)
def process_args(argparser):
    argparser.add_argument('--target', type=str, dest='to_token', default=config.ETH_CONTRACT_ADDRESS, help='address of destination token (default: reserve token)')
    argparser.add_argument('-d', const=True, action='store_const', help='dry-run: No converion, only return price')
    argparser.add_argument('from_token', type=str, help='address of source token')
    argparser.add_argument('amount', type=int, help='amount of source token to convert')



def execute(session, eargs):

    path_finder_address = session.registry.get_contract_address('BancorNetworkPathFinder')
    network_address = session.registry.get_contract_address('BancorNetwork')
    converter_registry_address = session.registry.get_contract_address('BancorConverterRegistry')
 
    network_contract = session.registry.get_contract_by_address(network_address)
  
    source_token_address = session.w3.toChecksumAddress(eargs.from_token)
    destination_token_address = config.ETH_CONTRACT_ADDRESS
    if eargs.to_token != None:
        destination_token_address = session.w3.toChecksumAddress(eargs.to_token)


    # generate the conversion path
    conversion_path = session.registry.get_contract_function(path_finder_address, 'generatePath')(source_token_address, destination_token_address).call()
    session.logger.info('conversion from {} to {} path {}'.format(source_token_address, destination_token_address, conversion_path))


    # dry run, don't do any conversions, just return price
    if eargs.d != None:
        session.logger.debug('dry-run, amount only')
        amount = session.registry.get_contract_function(network_address, 'getReturnByPath')(conversion_path, eargs.amount).call()
        print(str(amount[0]))
        return

    # set transfer approval for source token
    # TODO: conceal contract mapping to indexing component
    token_abi = session.registry.get_contract_abi('ERC20Token')
    source_token_converter_id = 'smart_token_{}'.format(eargs.from_token)
    session.registry.register_contract_by_name(source_token_converter_id, source_token_address, token_abi)


    # TODO: only set if not 0? (atomicity may require wrapping both calls in one in a contract)
    args = [network_address, 0]
    txid = session.persistence.register_transaction()
    session.processor.process_function_transaction(txid, source_token_address, source_token_converter_id, 'approve', args=args, gas_limit=config.ETH_GAS_LIMIT)
    args = [network_address, eargs.amount] 
    txid = session.persistence.register_transaction()
    session.processor.process_function_transaction(txid, source_token_address, source_token_converter_id, 'approve', args=args, gas_limit=config.ETH_GAS_LIMIT)



    # perform the conversion
    # TODO set with get return by path by overrideable by arg
    expected_return = 1
    args = [conversion_path, eargs.amount, expected_return, config.WITHDRAW_TO_ADDRESS, config.ZERO_ADDRESS, 0]
    txid = session.persistence.register_transaction()
    session.processor.process_function_transaction(txid, network_address, 'bancor_network', 'claimAndConvertFor2', args=args, gas_limit=config.ETH_GAS_LIMIT)
    hsh = session.persistence.transactions[txid]['hash']
    rcpt = session.processor.wait_for_transaction_receipt(txid)
    ev = network_contract.events.Conversion().processReceipt(rcpt)
    session.logger.info('event {}'.format(ev))
    result_amount = ev[0].args['_toAmount']
    print(str(result_amount))
    return 
